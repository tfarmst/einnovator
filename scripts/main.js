$(function () { // wait for document ready
		// init
		var controller = new ScrollMagic.Controller();

		// define movement of panels
		var wipeAnimation = new TimelineMax()
        // animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})		
			.to("#slideContainer2", 1,   {x: "100%", ease: 10})	
			.to("#slideContainer3", 1, {x: "100%"})				
            .to("#slideContainer4", 1, {x: "100%;"})
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})		
			.to("#slideContainer2", 1,   {x: "80%", ease: 10})	
			.to("#slideContainer3", 1, {x: "100%"})				
            .to("#slideContainer4", 1, {x: "100%;"})
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "70%"})
			.to("#slideContainer3", 1, {x: "75%"})
            .to("#slideContainer4", 1, {x: "50%;"})
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "60%"})
			.to("#slideContainer3", 1, {x: "70%"})
            .to("#slideContainer4", 1, {x: "25%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "50%"})
			.to("#slideContainer3", 1, {x: "55%"})
            .to("#slideContainer4", 1, {x: "0%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "40%"})
			.to("#slideContainer3", 1, {x: "40%"})
            .to("#slideContainer4", 1, {x: "0%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "30%"})
			.to("#slideContainer3", 1, {x: "0%"})
            .to("#slideContainer4", 1, {x: "0%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {z: -1, x: "20%"})
			.to("#slideContainer3", 1, {x: "0%"})
            .to("#slideContainer4", 1, {x: "0%;"})
             // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "10%"})
			.to("#slideContainer3", 1, {x: "0%"})
            .to("#slideContainer4", 1, {x: "0%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-00%"})
            .to("#slideContainer4", 1, {x: "0%;"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-20%"})
            .to("#slideContainer4", 1, {x: "0%;"})
             // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-30%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-40%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-50%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-60%"})
            .to("#slideContainer4", 1, {x: "100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-70%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-80%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-90%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-120%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-130%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-140%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-150%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-160%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-170%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-180%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-190%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-200%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            .to("#slideContainer4", 1, {x: "-200%"});
            
            

		// create scene to pin and link animation
		new ScrollMagic.Scene({
				triggerElement: "#pinContainer",
				triggerHook: .07,
				duration: "4000" // 4000
			})
			.setPin("#pinContainer")
			.setTween(wipeAnimation)
            // .addIndicators()
			.addTo(controller);
            

             $('body').on('load', '#twitter-widget-0', function() {  
    //$("#twitter-widget-0").contents().find('head').append('<style>' + '::-webkit-scrollbar-track {background: #646464;}::-webkit-scrollbar-thumb{background:#000; }::-webkit-scrollbar {    background: transparent;width: 16px;}' + '</style>'); 
    alert('test');
    console.log('test');
  });




	});
    
    
    
