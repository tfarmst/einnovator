var controller = new ScrollMagic.Controller();

    var slider = function(){
		// define movement of panels
		var wipeAnimation = new TimelineMax()
            // animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})		
			.to("#slideContainer2", 1,   {x: "20%"})	
			.to("#slideContainer3", 1, {x: "100%"})				
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})		
			.to("#slideContainer2", 1,   {x: "15%"})	
			.to("#slideContainer3", 1, {x: "100%"})				
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "10%"})
			.to("#slideContainer3", 1, {x: "75%"})
			// animate to next panel
			.to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "5%"})
			.to("#slideContainer3", 1, {x: "70%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "55%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-20%"})
             // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-30%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-40%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-50%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-60%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-70%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-80%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-90%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})
            // animate to next panel
            .to("#slideContainer1", 1, {x: "0%"})
			.to("#slideContainer2", 1,   {x: "0%"})
			.to("#slideContainer3", 1, {x: "-100%"})

            
            

		// create scene to pin and link animation
		new ScrollMagic.Scene({
				triggerElement: "#pinContainer", 
				triggerHook: .07, //.07
				duration: "3000" // was 4000, now 3000
			})
			.setPin("#pinContainer")
			.setTween(wipeAnimation)
            .addIndicators()
			.addTo(controller);
    };
	
    scene.on("start", function (event) {
    console.log("Hit start point of scene.");
});
    
    
